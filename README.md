# Instagram Crawler

## Requirements

* Python 3.7
* boto3
* InstagramApi
* python-dotenv
* requests

(see also requirements.txt)

## Installation

In case you do not have `pip` installed, first you should do:
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py

rm get-pip.py
```
then execute the following comands:
```bash
python3 -m venv ./venv/
source ./venv/bin/activate

pip3 install --upgrade pip
pip3 install -r requirements.txt
pip3 install -e .
```

### .env Variables

Create a `.env` file (or set the corresponding environment variables manually, if you prefer) if the following
variables. (see file `.env.example`)

```dotenv
AWS_ACCESS_KEY=aws-access-key
AWS_SECRET_KEY="aws-secret"
S3_BUCKET=my-bucket-name
TABLE_NAME=my_dynamodb_table_name
IG_USER=my_ig_user
IG_PASS="myP4ssW0rd"

```

## Usage examples

### Basic usage from the command line
```bash
python main.py username1 username2 username3
```

### Basic usage saving output as json file (additionally to DynamoDB)
```bash
python main.py username1 username2 username3 --output-dir=/path/to/dir
```

### Programmatic Usage

The same kind of usage described above can be performed programmatically:

```python
from instapy.instagram import Instagram
from instapy.settings import IG_PASS, IG_USER

usernames = {
    'so.kozi',
    'https://www.instagram.com/upperleftliving/',
    'seattleseekers',
}
with Instagram(IG_USER, IG_PASS) as ig:
    ig.crawl_user_data(usernames)
```

*Note*: User names can be passed either as instagram handles (e.g.: `upperleftliving`)
or as an url (e.g.: `https://www.instagram.com/upperleftliving`). 

*Note 2*: When logging in through the Instagram API, you might see the following errors:
```bash
Fail to import moviepy. Need only for Video upload.
Request return 429 error!
{'message': 'Please wait a few minutes before you try again.', 'status': 'fail'}
Request return 404 error!
```
These errors can be ignored. They are caused when Python tries to import the package `moviepy`.
This package would be necessary only in cases when we would like to upload videos to IG.

## App Overview

1) Given public user profiles(e.g. `so.kozi/`,
`https://www.instagram.com/upperleftliving/`, `https://www.instagram.com/seattleseekerss/`, etc.)
extract all their posts.

2) For each post, download any images for storage in an S3 bucket and extract
`user_id`, `link`, `posted_on` (timestamp), `captions`, `like_count`, `comment_count` & `follower_count`, and a few others.

Example:

```json
{
        "media_id": 2009962533801444777,
        "user_id": 2940370544,
        "follower_count": 28865,
        "media_type": 1,
        "posted_on": 1553826225717366,
        "fetched_on": 1553980790,
        "comment_count": 17,
        "like_count": 960,
        "caption": "The waterfront at downtown Seattle has stunning views over West Seattle and the Puget Sound, and it's a fun place to walk and explore. There are places to sit and enjoy the view, places to enjoy a quick snack, and places for fine dining. You can watch the ferries and container ships travel across Elliott Bay, or shop at unique souvenir and novelty shops, art galleries, and a family-friendly arcade. You'll see sea mammals and creatures of all kinds, whether at the Seattle Aquarium or along the shoreline. And best of all, much of the activity is free || Lovely photo taken by @chrisgeringphoto || Join us in exploring the Upper Left by tagging your adventures to #upperleftliving || #seattle #spaceneedle #skyline",
        "hashtags": [
            "seattle",
            "skyline",
            "spaceneedle",
            "upperleftliving"
        ],
        "mentions": [
            "chrisgeringphoto"
        ],
        "images": [
            "https://s3.amazonaws.com/my-bucket/54732133_385086168710407_1575035633383357384_n.jpg"
        ],
        "link": "https://www.instagram.com/p/Bvk0kZ3FEWp"
    }
```

3) For each caption, parse it and store any @ mentions & hashtags.

4) Save the JSON object in a DynamoDB table, including a reference to the url of the image saved to S3.

## Author

* Jay Rod, 2019 (jayrod@tuta.io)