import logging
import os
import pprint

import boto3

from instapy.settings import S3_BUCKET, AWS_ACCESS_KEY, AWS_SECRET_KEY, TABLE_NAME

logger = logging.getLogger(__name__)

session = boto3.Session(
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_KEY,
)

s3 = session.resource('s3')
db = session.resource('dynamodb')
table = db.Table(TABLE_NAME)

try:
    # This url might contain the region, if region US East (N. Virginia).
    _S3_ENDPOINT_URL = s3.meta.client.meta.endpoint_url
except AttributeError:
    raise


def get_public_url(key):
    return f'{_S3_ENDPOINT_URL}/{S3_BUCKET}/{key}'


def s3_upload(img, img_url):
    key = os.path.basename(img_url).split('?', 1)[0]
    s3.Bucket(S3_BUCKET).put_object(Body=img, Key=key, ContentType='image/jpeg')
    return get_public_url(key)


def dynamodb_batch_add(items):
    with table.batch_writer() as batch:
        for i in items:
            try:
                batch.put_item(Item=i)
            except Exception as e:
                logger.error(f'Item {pprint.pformat(i, indent=2)} could not be saved to DynamoDB due to: {e}')
