import logging
import time

logger = logging.getLogger(__name__)


def timeit(func):
    def timed(*args, **kw):
        ts = time.time()
        result = func(*args, **kw)
        te = time.time()
        logger.info(f'{func.__name__} {(te - ts) * 1000:2.2f} ms')
        return result

    return timed
