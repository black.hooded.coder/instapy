import os

from dotenv import load_dotenv

load_dotenv()

AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY = os.getenv('AWS_SECRET_KEY')
S3_BUCKET = os.getenv('S3_BUCKET')
TABLE_NAME = os.getenv('TABLE_NAME')

IG_USER = os.getenv('IG_USER')
IG_PASS = os.getenv('IG_PASS')
