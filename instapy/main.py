import logging

import fire

from instapy.instagram import Instagram
from instapy.settings import IG_PASS, IG_USER

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def crawl(*usernames, output_dir=None):
    with Instagram(IG_USER, IG_PASS) as ig:
        ig.crawl_user_data(usernames, output_dir)


if __name__ == '__main__':
    fire.Fire(crawl)
