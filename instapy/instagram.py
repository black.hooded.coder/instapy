import json
import logging
import os
import time

import requests
from InstagramAPI import InstagramAPI

from instapy.s3_utils import s3_upload, dynamodb_batch_add
from instapy.utils import timeit

logger = logging.getLogger(__name__)


class Instagram:
    def __init__(self, user, password):
        self.api = InstagramAPI(user, password)
        self.api.login()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val is not None:
            logger.error(exc_tb)
            logger.error(exc_type)
            logger.error(exc_val)
        self.close()

    def close(self):
        self.api.logout()

    @staticmethod
    def get_images(post):
        if 'image_versions2' in post:  # Post with single image.
            # Get first image which is the high resolution one.
            return [post.get('image_versions2').get('candidates')[0].get('url')]
        elif 'carousel_media' in post:  # Post with more than one image.
            return [media.get('image_versions2').get('candidates')[0].get('url') for media in
                    post.get('carousel_media')]

    @staticmethod
    def upload_images(images):
        try:
            urls = []
            for img_url in images:
                res = requests.get(img_url, stream=True).raw.read()
                urls.append(s3_upload(res, img_url))
            logger.debug('Upload successful!')
            return urls
        except Exception as e:
            logger.error(e)

    @staticmethod
    def _get_words_by_leading_char(text, char):
        return sorted(list({i.strip(char).strip() for i in text.split() if i.startswith(char)}))

    @staticmethod
    def get_hash_tags(text):
        return Instagram._get_words_by_leading_char(text, '#')

    @staticmethod
    def get_mentions(text):
        return Instagram._get_words_by_leading_char(text, '@')

    @staticmethod
    @timeit
    def process_post(user, posts, upload_to_s3=True):
        new_posts = []
        for p in posts:
            try:
                caption = p.get('caption', {}).get('text', '')
            except AttributeError:
                caption = '*no caption*'
            public_urls = Instagram.get_images(p)
            if upload_to_s3:
                public_urls = Instagram.upload_images(public_urls)
            new_post = {
                'media_id': p['pk'],
                'user_id': user.get('pk'),
                'follower_count': user.get('follower_count'),
                'media_type': p['media_type'],
                'posted_on': p['device_timestamp'],
                'fetched_on': int(time.time()),
                'comment_count': p['comment_count'],
                'like_count': p['like_count'],
                'caption': caption,
                'hashtags': Instagram.get_hash_tags(caption),
                'mentions': Instagram.get_mentions(caption),
                'images': public_urls,
                'link': f'https://www.instagram.com/p/{p["code"]}'
            }
            new_posts.append(new_post)
        return new_posts

    def get_posts_for_user(self, username):
        result = self.api.searchUsername(username)
        if not result:
            return
        user = self.api.LastJson.get('user')
        posts = self.api.getTotalUserFeed(user.get('pk'))
        posts = Instagram.process_post(user, posts)
        return user, posts

    @staticmethod
    def write_userdata_to_json(username, posts, output_dir):
        with open(os.path.join(output_dir, f'{username}.json'), 'w') as fout:
            json.dump(posts, fout, indent=4, ensure_ascii=False, default=str)

    def crawl_user_data(self, usernames, output_dir=None):
        try:
            for username in usernames:
                username = username.strip('/').split('/')[-1]
                logger.info(f'Retrieving posts by "{username}"...')
                user, posts = self.get_posts_for_user(username)
                logger.info(f'Writing {len(posts)} posts by "{username}" to database...')
                dynamodb_batch_add(posts)
                if output_dir:
                    Instagram.write_userdata_to_json(username, posts, output_dir=output_dir)
        except Exception as e:
            logger.error(e)
