import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

format = '%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s'

logging.basicConfig(format=format,
                    datefmt='%Y-%m-%d:%H:%M:%S',
                    level=logging.WARNING)
